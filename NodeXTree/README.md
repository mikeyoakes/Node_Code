# SCOPEAR
ScopAR Repository.
-----------------------------------------
XMAS TREE and X GENERATOR applications,
Written in JavaScript(Node.js),
By Michael Oakes.
-----------------------------------------

Install Node.js and install the Mocah and Sinon) modules:

	npm install -g mocha
	npm install -g sinon



=========
XMAS-TREE
=========
I have implemented 2 versions of the Xmas Tree code. One using a recursive function, and another more modular implementation:

MODULAR VERSION
---------------

ADJUST HEIGHT:	To edit the height of the generated christmas tree, adjust the value of the treeheight variable in the xmas_main.js file and save the file.


RUN:			From inside the xmas directory, use the following command to run the application:

					node xmas_main.js

					
TEST: 			From inside the xmas directory, use the following command to run the Unit Tests:  (The Unit Tests are in the Test directory)

					mocha test

					
					
					
					
RECURSIVE VERSION
-----------------
I added a quick alternative implementation using a recursive function that can be viewed and run as below.  I didn't create unit tests for this version but did for the other two applications.

ADJUST HEIGHT:	To edit the height of the generated christmas tree, adjust the value of the treeheight variable in the xmas_main_recursive.js file and save the file.


RUN:			From inside the xmas directory, use the following command to run recursive version the application:

					node xmas_main_recursive.js
				
					
					
					


-------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------

===========
X GENERATOR
===========
For the implementation of the 'X' genrator code:

ADJUST HEIGHT:	To edit the height of the generated 'X', adjust the value of the xheight variable in the node x_main_first_class.js file and save the file.


RUN:			From inside the x directory, use the following command to run the application from the xmas directory:

					node x_main_first_class.js

					
TEST: 			From inside the x directory, use the following command to run the Unit Tests:  (The Unit Tests are in the Test directory)

					mocha test





