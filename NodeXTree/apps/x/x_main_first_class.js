var xgen = require('./lib/generation-x');

// Change the value of the xhieght parameter variable
// below to adjust the height of the generated 'X'.
var xheight = 7;

// Call to main program to generate the 'X'.
xgen.x(xheight);