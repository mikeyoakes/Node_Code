// Import the xmas package to allow reuse of the print function 
// instead of duplicating this functionality.
var xmas = require('../../xmas/lib/xmas');


exports.populate_display_lines = function populate_display_lines(line_number, num_of_columns){
    
    // Initialize variable for the current line.
    var return_line = "";
    // Only process lines if there are lines to process,
    // otherwise just return an empty string.
    if (num_of_columns >=0)
	{    
        // Set column counter for first column.
        var curr_column = 1;
        // Loop through and generate appropriate output
        // for each column in turn.
        while (curr_column <= num_of_columns)
        {
            // Characters that outline the X are only displayed in the matrix of characters where
            // the column and line number are equal, or when the line number added to the column number
            // added together are equal to the value of the number of columns in the matrix +1.
            // When this condition is matched then we output a star character, otherwise we output a space.
            if ( (line_number == curr_column) || (line_number + curr_column) == (num_of_columns + 1) )  {
                return_line += '*';        
            }
            else {           
                return_line += ' ';
            }
             curr_column++;
        }    
	}
	return return_line;
}



exports.x = function x(x_height){
    
    
        if (isNaN(x_height) || (!Number.isInteger(x_height)) ){
            xmas.print_output("Please supply a valid numerical integer for the height parameter.");
        }
        else {
    
        // Variable that counts the current line;
        var line_number  = 1;   
    
        // Variable to hold the number of coluns
        // acroos the X. The x is a square so this 
        // will be equal to the hight value.
        var no_of_columns = (x_height);
    
        // Array to hold each line of output to be displayed.
        var display_lines = [];

        // Run through a switch statement to allow
        // for handling of 2 special cases - heigh to 1 and 2
        // which don't match the pattern for all the other processing.
        // Set the more common x generation processing as the default.
            switch(true){
                case (x_height==1) : 
                    display_lines[0] = "X";
                    break;
                case (x_height==2) :          
                    display_lines[0] = "\\/";
                    display_lines[1] = '/\\';
                    break;
                default:      
                    for (iterator = 0; iterator < x_height; iterator++, line_number++) {     
                    display_lines[iterator] =  module.exports.populate_display_lines(line_number,no_of_columns);
                    }
        }
        // Call function to print out all of the display lines to the console.
        xmas.print_display_lines(display_lines);
            
    }
}




