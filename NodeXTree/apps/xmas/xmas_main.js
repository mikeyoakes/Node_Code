var xmas = require('./lib/xmas');

// Change the value of the treehieght parameter variable
// below to adjust the height of the generated christmas tree.
var treeheight = 10;

// Call to main program to generate christmas tree.
xmas.xmas_tree(treeheight);