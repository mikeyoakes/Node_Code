exports.populate_display_lines = function populate_display_lines(num_of_lines, num_of_spaces){
    

    var return_line = "";
    
	if (num_of_spaces >=0)
	{

		outside = '';
        current_num_of_spaces = num_of_spaces;
		while (current_num_of_spaces > 0 ) {
			outside += ' ';
			current_num_of_spaces --;
		}


		middle = '';
		line_length = ((num_of_lines + num_of_spaces) * 2 ) - 1;
		num_of_chars = line_length - (num_of_spaces * 2);

        while (num_of_chars > 0 ) {
			middle += '*';
			num_of_chars --;
		}				
		return_line = (  outside +  middle +  outside);   
	}
	return return_line;
}


exports.print_output = function print_output(output_text){
    console.log(output_text);   
}

exports.print_display_lines = function print_display_lines(display_lines){
    
    //  Print out each line from the array of display lines
    //  passed to this function.
    for (line in display_lines) {
        module.exports.print_output(display_lines[line]);
    }
}

exports.xmas_tree = function print_tree(tree_height){
    
        if (isNaN(tree_height) || (!Number.isInteger(tree_height)) ){
            module.exports.print_output("Please supply a valid numerical integer for the height parameter.");
        }
        else {
    
        // Variable that counts the current line;
        var count_of_lines  = 1;   
    
        // Variable to hold the number of spaces required
        // at the start and end of each line.
        var no_of_spaces = (tree_height-1);
    
        // Array to hold each line of output to be displayed.
        var display_lines = [];
    
        // Iterate through each line can call function to create 
        // each display line of output.
        for (iterator = 0; iterator < tree_height; iterator++, count_of_lines++, no_of_spaces--) {
            display_lines[iterator] =  module.exports.populate_display_lines(count_of_lines,no_of_spaces);
        }
    
        // Call function to print out all of the display lines to the console.
        module.exports.print_display_lines(display_lines);
    }
}




