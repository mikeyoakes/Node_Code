exports.print_tree_lines = function print_tree_lines(num_of_lines, num_of_spaces){
    

	if (num_of_spaces >=0)
	{

		outside = "";
        current_num_of_spaces = num_of_spaces;
		while (current_num_of_spaces > 0 ) {
			outside += " ";
			current_num_of_spaces --;
		}


		middle = "";
		line_length = ((num_of_lines + num_of_spaces) * 2 ) - 1;
		num_of_chars = line_length - (num_of_spaces * 2);

        while (num_of_chars > 0 ) {
			middle += "*";
			num_of_chars --;
		}				
		console.log( outside + middle + outside);
		print_tree_lines(++num_of_lines, --num_of_spaces); 
       
	}
	else return;

}



exports.print_tree = function print_tree(tree_height){
    
        if (isNaN(tree_height) || (!Number.isInteger(tree_height)) ){
            console.log("Please supply a valid numerical integer for the height parameter.");
        }
        else {
            module.exports.print_tree_lines(1, (tree_height - 1));
        }
}
