const sinon  = require('sinon');
const assert = require('assert');

var xmas = require('../lib/xmas');



// Test 0 - For an invalid height of "a" - should return a customized user error message- character.
it('Should log a customized user error message to the console - character', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree('a');

  // assert that it was called with the correct value
  assert(spy.calledWith("Please supply a valid numerical integer for the height parameter."));

  // restore the original function
  spy.restore();
});


// Test 1 - For an invalid height of "a" - should return a customized user error message.
it('Should log a customized user error message to the console - string', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree("a");

  // assert that it was called with the correct value
  assert(spy.calledWith("Please supply a valid numerical integer for the height parameter."));

  // restore the original function
  spy.restore();
});

  

// Test 2 - For an invalid height of 2.5 (should be an integer!) - should return a customized user error message.
it('Should log a customized user error message to the console - non integer.', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree(2.5);

  // assert that it was called with the correct value
  assert(spy.calledWith("Please supply a valid numerical integer for the height parameter."));

  // restore the original function
  spy.restore();
});



// Test 3 - For an invalid height of 2.5 (should be an integer!) - should return a customized user error message.
it('Should log a customized user error message to the console - null.', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree(null);

  // assert that it was called with the correct value
  assert(spy.calledWith("Please supply a valid numerical integer for the height parameter."));

  // restore the original function
  spy.restore();
});


// Test 4 - For an invalid height of no supplied height - should return a customized user error message.
it('Should log a customized user error message to the console - no value supplied', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree();

  // assert that it was called with the correct value
  assert(spy.calledWith("Please supply a valid numerical integer for the height parameter."));

  // restore the original function
  spy.restore();
});


// Test 5 - For a height of 1 - should return just a "*" character.
it('Should log a simple * to the console - 1', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree(1);

  // assert that it was called with the correct value
  assert(spy.callCount == 1);    
  assert(spy.calledWith("*"));

  // restore the original function
  spy.restore();
});


// Test 6 - For a height of 2.
it('Should draw a tree of height 2', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');       
    
  // call the function that needs to be tested
  xmas.xmas_tree(2);

  // assert that it was called with the correct values
  assert(spy.callCount == 2);
  assert(spy.calledWith(' * '));
  assert(spy.calledWith('***'));

  // restore the original function
  spy.restore();
});


// Test 8 - For a height of 3.
it('Should draw a tree of height 3', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree(3);

  // assert that it was called with the correct values
  assert(spy.callCount == 3);
  assert(spy.calledWith('  *  '));
  assert(spy.calledWith(' *** '));
  assert(spy.calledWith('*****'));
    
  // restore the original function
  spy.restore();
});



// Test 9 - For a height of 4.
it('Should draw a tree of height 4', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree(4);

  // assert that it was called with the correct values
  assert(spy.callCount == 4);
  assert(spy.calledWith('   *   '));
  assert(spy.calledWith('  ***  '));
  assert(spy.calledWith(' ***** '));
  assert(spy.calledWith('*******'));    
    
  // restore the original function
  spy.restore();
});


// Test 10 - For a height of 5.
it('Should draw a tree of height 5', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree(5);

  // assert that it was called with the correct value
  assert(spy.callCount == 5);
  assert(spy.calledWith('    *    '));
  assert(spy.calledWith('   ***   '));
  assert(spy.calledWith('  *****  '));
  assert(spy.calledWith(' ******* '));    
  assert(spy.calledWith('*********'));   

  // restore the original function
  spy.restore();
});




// Test 10 - For a height of 10.
it('Should draw a tree of height 10', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree(10);

  // assert that it was called with the correct value
  assert(spy.callCount == 10);
  assert(spy.calledWith('         *         '));
  assert(spy.calledWith('        ***        '));
  assert(spy.calledWith('       *****       '));
  assert(spy.calledWith('      *******      '));    
  assert(spy.calledWith('     *********     '));   
  assert(spy.calledWith('    ***********    '));
  assert(spy.calledWith('   *************   '));    
  assert(spy.calledWith('  ***************  ')); 
  assert(spy.calledWith(' ***************** ')); 
  assert(spy.calledWith('*******************'));    
    
  // restore the original function
  spy.restore();
});



// Test 11 - For a height of a negative number - should return nothing.
it('Should log nothing to the console - -1', () => {
  // "spy" on `console.log()`
  let spy = sinon.spy(console, 'log');

  // call the function that needs to be tested
  xmas.xmas_tree(-1);

  // assert that print was not called
  assert(spy.callCount == 0);    

  // restore the original function
  spy.restore();
});

